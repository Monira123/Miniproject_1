<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phonebook</title>

        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body><div class="container"><br>
            <form action="store.php" method="post" class="form-inline">
                <div class="form-group col-md-10">
                    <label for="title">User name:</label>
                    <input type="text" name="title" class="form-control" placeholder="User name"><br><br>

                    <label for="address">Address:</label>
                    <textarea name="address" class="form-control" rows="3"></textarea><br><br>

                    <label for="hphone">Home phone:</label>
                    <input type="text" name="hphone" class="form-control" placeholder="Home phone"><br><br>

                    <label for="mphone">Mobile No.:</label>
                    <input type="text" name="mphone" class="form-control" placeholder="Mobile number"><br><br>

                    <button type="submit" class="btn btn-primary">Submit</button><br><br>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>


