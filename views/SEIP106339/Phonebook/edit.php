<?php
session_start();
include_once ("../../../" . "vendor/autoload.php");

use \Ahsan\BITM\SEIP106339\Phonebook;
use \Ahsan\BITM\SEIP106339\Message;
use \Ahsan\BITM\SEIP106339\Utility;

$phonebook = new Phonebook();
//Utility::dd($students);
$phone = $phonebook->edit($_GET['id']);
//Utility::dd($student);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Edit</title>

        <!-- Bootstrap -->
        <link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body><div class="container"><br>
            <form action="update.php" method="post" class="form-inline">
                <div class="form-group col-md-10">
                    <input type="hidden" class="form-control" 
                           name="id"
                           id="id"
                           value="<?php echo $phone->id; ?>">
                    <label for="title">User name:</label>
                    <input type="text" name="title" class="form-control" placeholder="Student name"
                           value="<?php echo $phone->title; ?>"><br><br>
                    
                    <label for="address">Address:</label>
                    <textarea name="address" class="form-control" rows="3">
                     <?php echo $phone->address; ?></textarea><br><br>
                              
                    <label for="hphone">Home phone:</label>
                    <input type="text" name="hphone" class="form-control" 
                           value="<?php echo $phone->hphone; ?>"><br><br>
                    
                    <label for="mphone">Cell phone:</label>
                    <input type="text" name="mphone" class="form-control" 
                           value="<?php echo $phone->mphone; ?>"><br><br>
                    
                    <button type="submit" class="btn btn-primary">Update</button><br><br>
                </div>
            </form>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>

